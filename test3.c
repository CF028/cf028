#include<stdio.h>
#include<math.h>

int main()
{
	float a,b,c,D,den,r1,r2;
	printf("Enter the value of a,b and c of the quadratic eqaution\n");
	scanf("%f%f%f",&a,&b,&c);
	D=(b*b)-(4*a*c);
	den=2*a;
	if (a==0)
	{
		printf("The given equation is not a quadratic equation\n");
	}
	else if(D>0)
	{
		r1=(-b+sqrt(D))/den;
		r2=(-b-sqrt(D))/den;
		printf("The roots are real and distinct and the roots are %f and %f\n",r1,r2);
	}

	else if(D==0)
	{
		r1=(-b+sqrt(D))/den;
		r2=(-b-sqrt(D))/den;
		printf("The roots are real and equal %f and %f\n",r1,r2);
	}
	else 
	{
		printf("The roots are imaginary\n");
	}
	return 0;
}
